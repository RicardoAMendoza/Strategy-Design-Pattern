# Strategy-Design-Pattern 
# =======================


L'objectif de ce projet est d'Identifier une famille d'algorithmes en tant que technologie de groupe, les rassembler et les rendre interchangeables.


## Environnement de travail.
* **Microsoft .NET**
* **Langage de programmation C#.**
* **Programmation orienté objet.**
 
## Concepts.
- Héritage.
- Protected.
- Abstract.
- Static.
- Events.
- Collections.

## Versions et gestionnaire de source 

Ce projet utilise Gitlab.com comme gestionnaire de source dans le dépôt suivant:
 
https://gitlab.com/RicardoAMendoza/Strategy-Design-Pattern

Historique de versions : https://gitlab.com/RicardoAMendoza/Strategy-Design-Pattern/commits/master

## Auteur

* **Ricardo Mendoza** 

## Licence

Ce projet utilise les licences suivantes:
-  the MIT License (MIT)